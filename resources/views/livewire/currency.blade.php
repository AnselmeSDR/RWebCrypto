<div>
    <select wire:model="currency" wire:change="updateCurrency($event.target.value)"
            class="form-control bg-dark text-white"
            name="select_currency"
            id="select_currency"
    >
        @foreach($currencies as $key)
            @if ($key != $currency)
                <option value="{{ $key }}">{{ $key }}</option>
            @else
                <option value="{{ $key }}" selected>{{ $key }}</option>
            @endif
        @endforeach
    </select>
</div>
