<div class="p-3 mt-5">
    <div class="utils-bar">
        <h2>Top 100</h2>
        <input class="form-control text-right bg-dark text-white" style="width: 30%" type="text" id="search" placeholder="Search">
    </div>
    <div class="table-responsive" style="background-color: #18191c">
        <table class="table" id="table">
            <thead>
            <tr class="text-right">
                <th></th>
                <th class="text-left">Name</th>
                <th>Price</th>
                <th>24h %</th>
                <th>Market Cap</th>
                <th>Volume</th>
                <th>Circulating Supply</th>
            </tr>
            </thead>

            @foreach($coins as $value => $coin)
                <tbody wire:poll.30000ms class="text-right">
                <tr>
                    <td>
                        <div class="d-flex">
                            <div class="rank p-2 rounded">
                                <span class="text-white px-2 font-weight-bold">{{ $coin->market_cap_rank }}</span>
                            </div>
                        </div>
                    </td>
                    <td class="text-left">
                        <img src="{{ $coin->image }}" alt="{{ $coin->image }}" class="icons">
                        <span class="pl-2">
                            <a style="color: white" href="/coin/{{ $coin->coin_id }}">
                                [{{ $coin->symbol }}]{{ $coin->name }}
                            </a>
                        </span>
                    </td>
                    <td>{{ $coin->current_price }}</td>

                    @if($coin->price_change_percentage_24h < 0)
                        <td class="red bi bi-caret-down-fill">
                            {{ -$coin->price_change_percentage_24h }}%
                        </td>
                    @else
                        <td class="green bi bi-caret-up-fill">
                            {{ $coin->price_change_percentage_24h }}%
                        </td>
                    @endif

                    <td>${{ $coin->market_cap }}</td>
                    <td>{{ $coin->total_volume }}</td>
                    <td>{{ $coin->circulating_supply . ' ' . $coin->symbol }}</td>
                </tr>
                </tbody>
            @endforeach
        </table>
    </div>
</div>

<style scoped>
    .utils-bar {
        display:         flex;
        justify-content: space-between;
        align-items:     center;
        margin-bottom:   1em;
    }

    .table div.text-muted {
        font-size:     0.85rem;
        font-weight:   600;
        margin-bottom: 0.3rem;
        margin-top:    0.3rem;
    }

    .table thead th {
        border: none;
    }

    .table tbody tr {
        border-top: 2px solid #222431 !important;
    }

    .table tr td {
        vertical-align: middle;
    }

    .rank {
        background-color: #222431;
    }
</style>

<script>
  let $rows = $('#table tr')

  $('#search')
    .keyup(function () {
      let val = $.trim($(this)
        .val())
        .replace(/ +/g, ' ')
        .toLowerCase()

      $rows.show()
        .filter(function () {
          let text = $(this)
            .text()
            .replace(/\s+/g, ' ')
            .toLowerCase()
          return !~text.indexOf(val)
        })
        .hide()
      $rows.filter(':eq(0)')
        .show()
    })
</script>
