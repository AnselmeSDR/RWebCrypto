<div wire:poll.30000ms>
    <div class="elem">
        <div class="box ml-2">
            <br>
            <div class="title">
                <h2>
                    <img class="icons" src="{{ $coin->image }}" alt="{{ $coin->image }}">
                    {{ $coin->name }}
                    <span class="ml-2 badge">{{ $coin->symbol }}</span>
                </h2>
            </div>
            <div class="groupPill">
                <div class="pill rank">Rank #{{ $coin->market_cap_rank }}</div>
                <div class="pill">Coin</div>
                @if($coin->link)
                    <a href="{{ $coin->link }}">
                        <div class="pill bi bi-link"> {{ $coin->showLink }}
                            <i class="pill bi bi-box-arrow-up-right"></i>
                        </div>
                    </a>
                @endif
                @if($coin->sourceLink)
                    <a href="{{ $coin->sourceLink }}">
                        <div class="pill bi bi-code">
                            Source code <i class="pill bi bi-box-arrow-up-right"></i>
                        </div>
                    </a>
                @endif
                @if($coin->redditLink)
                    <a href="{{ $coin->sourceLink }}">
                        <div class="pill bi bi-code">
                            Reddit <i class="pill bi bi-box-arrow-up-right"></i>
                        </div>
                    </a>
                @endif
            </div>
        </div>

        <div class="box mr-2 text-right">
            <div>{{ $coin->name }} Price ({{ $coin->symbol }})</div>
            <div class="price">
                <h2><em class="mr-1">{{ session('currency') }}</em>
                    {{ $coin->price }}
                    @if($coin->market_data['price_change_percentage_24h'] < 0)
                        <div class="badge red bi bi-caret-down-fill">
                            {{ -$coin->price_change_percentage_24h_in_currency }}%
                        </div>
                    @else
                        <div class="badge green bi bi-caret-up-fill">
                            {{ $coin->price_change_percentage_24h_in_currency }}%
                        </div>
                    @endif
                </h2>
            </div>
            <div>Price low:
                <b>{{ (new App\Http\Livewire\Coin)->formatPrice($coin->price_low_24h) }}</b> {{ session('currency') }}
                <progress max="{{ $coin->price_high_24h }}"
                          value="{{ $coin->price }}"
                >
                </progress>
                 Price high:
                <b>{{ (new App\Http\Livewire\Coin)->formatPrice($coin->price_high_24h) }}</b> {{ session('currency') }}
            </div>

        </div>

    </div>
    <div class="desc m-2">
        {!! $coin->description !!}
    </div>
    <hr>
    <div class="elem">
        <div class="box-light m-2">
            Market Cap:
            <br>
            <b>{{ $coin->market_cap_24h_in_currency }}</b> {{ session('currency') }}
            <br>
            @if($coin->market_data['price_change_percentage_24h'] < 0)
                <div class="badge red bi bi-caret-down-fill">
                    {{ -$coin->market_cap_change_percentage_24h_in_currency }}%
                </div>
            @else
                <div class="badge green bi bi-caret-up-fill">
                    {{ $coin->market_cap_change_percentage_24h_in_currency }}%
                </div>
            @endif
        </div>
        <div class="box-light m-2">
            Price change (24h):
            <br>
            <b>{{ $coin->price_change_24h_in_currency }}</b>
            <br>
            @if($coin->market_data['price_change_percentage_24h'] < 0)
                <div class="badge red bi bi-caret-down-fill">
                    {{ -$coin->price_change_percentage_24h_in_currency }}%
                </div>
            @else
                <div class="badge green bi bi-caret-up-fill">
                    {{ $coin->price_change_percentage_24h_in_currency }}%
                </div>
            @endif
        </div>
        <div class="box-light m-2">
            Volume <b>(24h)</b>:
            <br>
            <b>{{ $coin->total_volume_24h_in_currency }}</b> {{ session('currency') }}
        </div>
    </div>
    <hr>
    <div class="elem">
        <div class="box-light-secondary m-2 mb-0">
            Circulating Supply
            <h4>
                <b>{{ $coin->circulating_supply }}<em class="mr-1"> {{ session('currency') }}</em></b>
            </h4>
        </div>
        <div class="box-light-secondary m-2 mb-0 text-right">
            <br>
            {{ $coin->circulating_supply_percentage ?? 'N/A' }}%
        </div>
    </div>
    <div>
        <div class="box-light-secondary m-2" style="width: auto; text-align: center;">
            <progress max="{{ $coin->max_supply }}"
                      value="{{ $coin->total_supply }}"
            >
            </progress>
        </div>
    </div>
    <div class="elem">
        <div class="box-light-secondary m-2">
            Max Supply
            <br>
            Total Supply
        </div>
        <div class="box-light-secondary m-2 text-right">
            {{ number_format($coin->max_supply) ?? 'N/A' }}
            <br>
            {{ number_format($coin->total_supply)  ?? 'N/A' }}
        </div>
    </div>
</div>

<style scoped>
    .elem {
        max-width:        inherit;
        display:          flex;
        justify-content:  space-between;
        flex-wrap:        wrap;
        background-color: transparent;
    }

    .box {
        padding:       1em;
        margin-bottom: 1em;
        width:         45%;
        height:        auto;
        min-width:     250px;
        border-radius: 10px;
    }

    .box-light {
        padding:       1em;
        margin-bottom: 1em;
        width:         30%;
        height:        auto;
        min-width:     250px;
        border-radius: 10px;
    }

    .box-light-secondary {
        padding:       1em;
        margin-bottom: 1em;
        width:         30%;
        height:        auto;
        min-width:     auto;
        border-radius: 10px;
    }

    .title {
        max-width: inherit;
        display:   flex;
        flex-wrap: wrap;
    }

    .title h2 {
        font-size:         32px;
        margin-right:      8px;
        display:           flex;
        -webkit-box-align: center;
        align-items:       center;
        font-weight:       700;
    }

    .title .badge {
        background-color: #323546;
        color:            #a1a7bb;
        border-radius:    4px;
        padding:          2px 6px;
        font-size:        12px;
        font-weight:      600;
        line-height:      18px;
        margin-left:      12px;
        margin-right:     6px;
    }

    .icons {
        height:       35px;
        width:        35px;
        margin-right: 12px;
    }

    .groupPill {
        display:    flex;
        flex-wrap:  wrap;
        box-sizing: border-box;
    }

    .pill {
        background-color: #323546;
        color:            #a1a7bb;
        border-radius:    4px;
        padding:          2px 6px;
        font-size:        11px;
        font-weight:      500;
        line-height:      18px;
        white-space:      nowrap;
        margin-right:     3px;
        margin-bottom:    3px;
    }

    .rank {
        background-color: #858ca2;
        color:            rgb(255, 255, 255);
    }

    .price h2 {
        font-size:         32px;
        line-height:       42px;
        margin-right:      8px;
        display:           flex;
        -webkit-box-align: center;
        justify-content:   flex-end;
        font-weight:       700;
    }

    .price .badge {
        white-space:   nowrap;
        border-radius: 8px;
        padding:       3px 10px;
    }

    progress {
        height:          5px;
        width:           90%;
        background:      #646b80;
        border-radius:   8px;
        text-align-last: center;
    }

    progress {
        color: #323646;
    }

    progress::-moz-progress-bar {
        background: #646b80;
    }

    progress::-webkit-progress-value {
        background: #323646;
    }

    progress::-webkit-progress-bar {
        background: #646b80;
    }

    .desc {
        color:     #a1a7bb;
        font-size: 11px;
    }

    @media (max-width: 520px) {
        .elem {
            justify-content: center;
        }

        .box {
            width: 100%;
        }

        .box-light {
            width: 100%;
        }

        .box-light-secondary {
            width: 45%;
        }
    }
</style>

