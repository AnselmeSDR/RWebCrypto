@extends('layouts.base')
@extends('layouts.navbar')

@section('body')
    @yield('content')

    @isset($slot)
        {{ $slot }}
    @endisset
@endsection
