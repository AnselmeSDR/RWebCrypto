<nav class="navbar navbar-dark" style="background-color: #18191c; padding: 1.5em">
    <a href="/">
        <img src="https://uploads-ssl.webflow.com/62f3ae125606d84b06b3051c/62f604c918d02fe807492ea7_Logo%20R%20Web%20blc.svg"
             alt="favicon" height="50px"
        >
        <span class="text-white" style="font-size: 16px;vertical-align: top;">CryptoCurrencies</span></a>

    <div class="text-right">
        @livewire('currency')
    </div>
</nav>
