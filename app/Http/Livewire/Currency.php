<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Currency extends Component {
    public array  $currencies = [
        'aed', 'ars', 'aud', 'bch', 'bdt', 'bhd', 'bmd', 'bnb', 'brl', 'btc',
        'cad', 'chf', 'clp', 'cny', 'czk', 'dkk', 'dot', 'eos', 'eth', 'eur',
        'gbp', 'hkd', 'huf', 'idr', 'ils', 'inr', 'jpy', 'krw', 'kwd', 'lkr',
        'ltc', 'mmk', 'mxn', 'myr', 'ngn', 'nok', 'nzd', 'php', 'pkr', 'pln',
        'rub', 'sar', 'sek', 'sgd', 'thb', 'try', 'twd', 'uah', 'usd', 'vef',
        'vnd', 'xag', 'xau', 'xdr', 'xlm', 'xrp', 'yfi', 'zar', 'bits', 'link',
        'sats'];
    public string $currency;

    public function mount() {
        $this->currency = session('currency', config('app.currency'));
    }

    public function updateCurrency($value) {
        session()->put('currency', $value);
        $this->emit('$refresh');
    }

    public function render() {
        return view('livewire.currency');
    }
}
