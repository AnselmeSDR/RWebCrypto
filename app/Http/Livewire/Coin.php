<?php

namespace App\Http\Livewire;

use App\Models\Coin as CoinModel;
use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Livewire\Component;

class Coin extends Component {

    protected $listeners = ['$refresh'];
    public $id_coin;

    /**
     * @throws Exception
     */
    public function render() : View {
        $coin = $this->getCoin($this->id_coin);

        return view('livewire.coin.coin', compact('coin'));
    }

    public function mount(Request $request) {
        $this->id_coin = last(explode('/', $request->url()));
    }

    /**
     * Get specific coin.
     *
     * @throws Exception
     */
    public function getCoin($id) : CoinModel {

        $client = new CoinGeckoClient();
        $response = $client->coins()->getCoin($id, ['tickers' => 'false', 'market_data' => 'true']);
        $coin = new CoinModel($response);

        $currency = session('currency') ?? config('app.currency');
        $toRemove = ['http://', 'https://', 'www.'];

        $coin->total_supply = $coin->market_data['circulating_supply'];
        $coin->max_supply = $coin->market_data['max_supply'];
        if ($coin->total_supply && $coin->max_supply)
            $coin->circulating_supply_percentage = number_format(($coin->total_supply / $coin->max_supply) * 100);
        $coin->circulating_supply = number_format($coin->circulating_supply,
                                                  strlen(substr(strrchr($coin->circulating_supply, "."),
                                                                1)) < 5 ? strlen(substr(strrchr($coin->circulating_supply,
                                                                                                "."), 1)) : 5);
        $coin->circulating_supply = self::formatPrice($coin->market_data['circulating_supply'], ',');
        $coin->current_price = self::formatPrice($coin->current_price) . ' ' . strtoupper($currency);
        $coin->description = $coin->description['en'] ?? 'No description available';
        $coin->image = $coin->image['small'] ?? $coin->image['large'] ?? $coin->image['thumb'];
        $coin->link = $coin->links['homepage'][0] ?? null;
        $coin->redditLink = $coin->links['subreddit_url'] ?? null;
        $coin->showLink = str_replace($toRemove, '', $coin->link);
        $coin->sourceLink = $coin->links['repos_url']['github'][0] ?? null;
        $coin->market_cap = number_format($coin->market_cap, 0, ',');
        $coin->market_cap_24h_in_currency = self::formatPrice($coin->market_data['market_cap'][$currency], ',');
        $coin->market_cap_change_percentage_24h_in_currency = number_format($coin->market_data['market_cap_change_percentage_24h_in_currency'][$currency],
                                                                            2);
        $coin->price = $coin->market_data['current_price'][$currency];
        $coin->price_change_24h = number_format($coin->price_change_percentage_24h, 2);
        $coin->price_change_24h_in_currency = self::formatPrice($coin->market_data['price_change_24h_in_currency'][$currency]) . ' ' . $currency;
        $coin->price_change_percentage_24h_in_currency = number_format($coin->market_data['price_change_percentage_24h_in_currency'][$currency],
                                                                       2);
        $coin->price_high_24h = $coin->market_data['high_24h'][$currency];
        $coin->price_low_24h = $coin->market_data['low_24h'][$currency];
        $coin->symbol = strtoupper($coin->symbol);
        $coin->total_volume = number_format($coin->total_volume, 0, ',');
        $coin->total_volume_24h_in_currency = self::formatPrice($coin->market_data['total_volume'][$currency], ',');

        return $coin;
    }

    public function formatPrice($price, ?string $decimal_separator = '.', ?string $thousands_separator = ',') : string {
        return number_format($price, strlen(substr(strrchr($price, '.'), 1)), $decimal_separator, $thousands_separator);
    }
}
