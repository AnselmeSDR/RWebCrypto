<?php

namespace App\Http\Livewire;

use App\Models\Coin;
use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class Coins extends Component {

    protected $listeners = ['$refresh'];

    /**
     * @throws Exception
     */
    public function render() : View {
        $coins = $this->getCoins();

        return view('livewire.coin.coins', compact('coins'));
    }

    /**
     * Get all coins.
     *
     * @return Collection
     * @throws Exception
     */
    public function getCoins() : Collection {
        $currency = session('currency') ?? config('app.currency');
        $client = new CoinGeckoClient();
        $response = $client->coins()->getMarkets($currency);
        $coins = (new Coin)->newCollection();

        foreach ($response as $data) {
            $data = array_merge($data, ['coin_id' => $data['id']]);
            $coins->push(new Coin($data));
        }

        foreach ($coins as $coin) {
            $coin->circulating_supply = number_format($coin->circulating_supply,
                                                      strlen(substr(strrchr($coin->circulating_supply, "."), 1))
                                                      < 5 ? strlen(substr(strrchr($coin->circulating_supply, "."),
                                                                          1)) : 5);
            $coin->current_price = self::formatPrice($coin->current_price) . ' ' . strtoupper($currency);

            $coin->market_cap = number_format($coin->market_cap, 0, ',');
            $coin->price_change_24h = number_format($coin->price_change_percentage_24h, 2);
            $coin->symbol = strtoupper($coin->symbol);
            $coin->total_volume = number_format($coin->total_volume, 0, ',');
        }

        return $coins;
    }

    public function formatPrice($price, ?string $decimal_separator = '.', ?string $thousands_separator = ',') : string {
        return number_format($price, strlen(substr(strrchr($price, '.'), 1)), $decimal_separator, $thousands_separator);
    }
}


