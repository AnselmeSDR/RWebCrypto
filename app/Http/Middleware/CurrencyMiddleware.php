<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CurrencyMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next) {
        $response = $next($request);

        if ($request->hasCookie('currency')) {
            return $response;
        }

        return $response->cookie()->forever('currency', config('app.currency'));
    }
}
