<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coin extends Model {
    use HasFactory;

    /**
     * @var array $fillable Data that can be modified by the user
     */
    protected $fillable = [
        'coin_id', 'symbol', 'name', 'image', 'current_price', 'categories', 'description', 'links',
        'market_cap', 'market_cap_rank', 'market_data',
        'market_cap_change_24h', 'market_cap_change_percentage_24h',
        'total_volume', 'high_24h', 'low_24h',
        'price_change_24h', 'price_change_percentage_24h', 'price_change_percentage_7d', 'price_change_percentage_14d',
        'price_change_percentage_30d', 'price_change_percentage_60d', 'price_change_percentage_200d', 'price_change_percentage_1y',
        'circulating_supply', 'total_supply', 'max_supply',
    ];
}
